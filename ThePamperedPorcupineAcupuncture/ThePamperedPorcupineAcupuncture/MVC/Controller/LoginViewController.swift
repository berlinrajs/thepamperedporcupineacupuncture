//
//  LoginViewController.swift
//  OptimaDentistry
//
//  Created by SRS Web Solutions on 12/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LoginViewController: MCViewController {
    
    @IBOutlet weak var textFieldUserName: MCTextField!
    @IBOutlet weak var textFieldPassword: MCTextField!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelPlace: UILabel!
    var isStaffLogin: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        labelVersion.text = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
        // Do any additional setup after loading the view.
        
        textFieldPassword.textFormat = .SecureText
        if isStaffLogin {
            textFieldUserName.placeholder = "USERNAME *"
            textFieldUserName.textFormat = .Username
            buttonBack?.isHidden = false
        } else {
            textFieldUserName.textFormat = .Email
            buttonBack?.isHidden = true
        }
        
        labelPlace.text = kPlace
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.string(from: Date()).uppercased()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonCancelAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func buttonSubmitAction() {
        if (textFieldUserName.isEmpty || !textFieldUserName.text!.isValidEmail) && !isStaffLogin  {
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        } else if textFieldUserName.isEmpty && isStaffLogin {
            self.showAlert("PLEASE ENTER THE USERNAME")
        } else if textFieldPassword.isEmpty {
            self.showAlert("PLEASE ENTER THE PASSWORD")
        } else {
            self.submitAction()
        }
    }
    
    func submitAction() {
        self.view.endEditing(true)
        if isStaffLogin {
            BRProgressHUD.show()
            PendingForm.staffLoginWithUsername(userName: textFieldUserName.text!.lowercased(), password: textFieldPassword.text!, completion: { (success, result, error) in
                BRProgressHUD.hide()
                if success {
//                    if let pendingForms = result {
//                        let pendingFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kPendingFormViewController") as! PendingFormViewController
//                        pendingFormVC.pendingForms = pendingForms
//                        self.navigationController?.pushViewController(pendingFormVC, animated: true)
//                    } else {
//                        self.showAlert("NO RECORDS FOUND", completion: {
//                            self.dismiss(animated: true, completion: nil)
//                        })
//                    }
                } else {
                    self.showAlert(error!.localizedDescription.uppercased())
                }
            })
        } else {
            ServiceManager.loginWithUsername(textFieldUserName.text!, password: textFieldPassword.text!) { (success, error) -> (Void) in
                if success {
                    UserDefaults.standard.set(true, forKey: kAppLoggedInKey)
                    UserDefaults.standard.setValue(self.textFieldUserName.text!, forKey: kAppLoginUsernameKey)
                    UserDefaults.standard.setValue(self.textFieldPassword.text!, forKey: kAppLoginPasswordKey)
                    UserDefaults.standard.synchronize()
                    (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                } else {
                    if error == nil {
                        self.showAlert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                    } else {
                        self.showAlert(error!.localizedDescription.uppercased())
                    }
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUserName {
            textFieldPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.submitAction()
        }
        return true
    }
}
