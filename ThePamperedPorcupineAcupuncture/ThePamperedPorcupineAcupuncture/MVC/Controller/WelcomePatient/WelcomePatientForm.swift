//
//  WelcomePatientForm.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/14/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class WelcomePatientForm: MCViewController {
    
    
    var signPatient : UIImage!
    var signPatient2 : UIImage!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var signaturePatient: UIImageView!
    
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var patientNameLabel2: UILabel!
    @IBOutlet weak var signaturePatient2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.text = patient.dateToday
        signaturePatient.image = signPatient
        patientNameLabel.text = patient.fullName
        
        labelDate2.text = patient.dateToday
        signaturePatient2.image = signPatient2
        patientNameLabel2.text = patient.fullName

        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
