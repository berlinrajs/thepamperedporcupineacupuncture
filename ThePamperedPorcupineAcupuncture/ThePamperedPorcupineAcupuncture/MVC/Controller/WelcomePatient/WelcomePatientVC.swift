//
//  WelcomePatientVC.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/14/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class WelcomePatientVC: MCViewController {
    
    @IBOutlet weak var signaturePatient: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var labelPatientName2: UILabel!
    @IBOutlet weak var signaturePatient2: SignatureView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate1.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        
        labelDate2.todayDate = patient.dateToday
        labelPatientName2.text = patient.fullName
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        
        if !signaturePatient.isSigned() || !signaturePatient2.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            
            let welcomePatientForm = consentStoryBoard
                .instantiateViewController(withIdentifier: "kWelcomePatientForm") as! WelcomePatientForm
            welcomePatientForm.patient = self.patient
            welcomePatientForm.signPatient = signaturePatient.signatureImage()
            welcomePatientForm.signPatient2 = signaturePatient2.signatureImage()
            self.navigationController?.pushViewController(welcomePatientForm, animated: true)
        }
    }
    
    
    
}
