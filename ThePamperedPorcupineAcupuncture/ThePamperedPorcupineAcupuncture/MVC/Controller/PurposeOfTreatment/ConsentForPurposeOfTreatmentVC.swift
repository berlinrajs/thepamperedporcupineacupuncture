//
//  ConsentForPurposeOfTreatmentVC.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/13/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class ConsentForPurposeOfTreatmentVC: MCViewController {
    
    @IBOutlet weak var signaturePatient: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate1.todayDate = patient.dateToday
    
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let consentForPurposeOfTreatmentFormObj = consentStoryBoard
                .instantiateViewController(withIdentifier: "kConsentForPurposeOfTreatmentForm") as! ConsentForPurposeOfTreatmentForm
            consentForPurposeOfTreatmentFormObj.patient = self.patient
            consentForPurposeOfTreatmentFormObj.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(consentForPurposeOfTreatmentFormObj, animated: true)
        }
    }
    
    
    
}
