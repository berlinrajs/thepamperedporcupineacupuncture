//
//  ConsentForPurposeOfTreatmentForm.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/13/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class ConsentForPurposeOfTreatmentForm: MCViewController {
    
    
    var signPatient : UIImage!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var patientNameLabel
    : UILabel!
    @IBOutlet weak var signaturePatient: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.text = patient.dateToday
        signaturePatient.image = signPatient
        patientNameLabel.text = patient.fullName
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
