//
//  AcupunctureInformedConsentVC.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/13/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class AcupunctureInformedConsentVC: MCViewController {
    
    
    
    @IBOutlet weak var signaturePatient: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var textFieldRelationship : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValue()
       

        // Do any additional setup after loading the view.
    }
    
    func loadValue()
    {
        labelDate1.todayDate = patient.dateToday

        
        if !patient.representativeRelationship.isEmpty
        {
            textFieldRelationship.text = patient.representativeRelationship
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValue()
    {
        patient.representativeRelationship = (textFieldRelationship.text?.isEmpty)! ? "" : textFieldRelationship.text!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let acupunctureConsentForm = consentStoryBoard
                .instantiateViewController(withIdentifier: "kAcupunctureInformedConsentForm") as! AcupunctureInformedConsentForm
            acupunctureConsentForm.patient = self.patient
            acupunctureConsentForm.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(acupunctureConsentForm, animated: true)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
