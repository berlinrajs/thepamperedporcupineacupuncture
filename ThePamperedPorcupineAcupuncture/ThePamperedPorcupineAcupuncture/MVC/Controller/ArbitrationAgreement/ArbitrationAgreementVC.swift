//
//  ArbitrationAgreementVC.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/13/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation


class ArbitrationAgreementVC: MCViewController {
    
    
    @IBOutlet weak var signatureInitial: SignatureView!
    @IBOutlet weak var signaturePatientOrRepresentative: SignatureView!
    @IBOutlet weak var signatureOffice: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var textFieldRelationship : MCTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
         loadValue()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()
    {
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        
        if !patient.representativeRelationship.isEmpty
        {
            textFieldRelationship.text = patient.representativeRelationship
        }
        
    }
    
    func saveValue()
    {
        patient.representativeRelationship = (textFieldRelationship
            .text?.isEmpty)! ? "" : textFieldRelationship.text!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        
        if !signaturePatientOrRepresentative.isSigned() || !signatureOffice.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            
            saveValue()
            let arbitratnAgreemntForm = consentStoryBoard
                .instantiateViewController(withIdentifier: "kArbitrationAgreementForm") as! ArbitrationAgreementForm
            arbitratnAgreemntForm.patient = self.patient
            arbitratnAgreemntForm.signPatient = signaturePatientOrRepresentative.signatureImage()
            arbitratnAgreemntForm.signDentist = signatureOffice.signatureImage()
            arbitratnAgreemntForm.signInitial = signatureInitial.signatureImage() == nil ? UIImage() : signatureInitial.signatureImage()
            self.navigationController?.pushViewController(arbitratnAgreemntForm, animated: true)
        }
    }
    
    
}
