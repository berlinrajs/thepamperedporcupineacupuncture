//
//  ArbitrationAgreementForm.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/13/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class ArbitrationAgreementForm: MCViewController {
    
    
    var signPatient : UIImage!
    var signDentist : UIImage!
    var signInitial : UIImage!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var labelRelationship: UILabel!
    @IBOutlet weak var patientNameLabel
    : UILabel!
    @IBOutlet weak var signaturePatient: UIImageView!
    @IBOutlet weak var signatureDentist: UIImageView!
    @IBOutlet weak var signatureInitial: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday

        signaturePatient.image = signPatient
        signatureDentist.image = signDentist
        signatureInitial.image = signInitial
        patientNameLabel.text = patient.fullName
        if !patient.representativeRelationship.isEmpty
        {
            labelRelationship.text = patient.representativeRelationship
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
