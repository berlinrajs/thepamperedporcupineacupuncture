//
//  ApicoectomyForm.swift
//  SmileDesignDentalSpa
//
//  Created by APPLE on 12/5/16.
//  Copyright © 2016 srs. All rights reserved.
//

import UIKit

class HipaaForm: MCViewController {
    
    @IBOutlet weak var patientSign: UIImageView!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var patientSignName: FormLabel!
    @IBOutlet weak var patientName: FormLabel!
    @IBOutlet weak var date: FormLabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        patientSignName.text = patient.fullName
        patientName.text = patient.fullName
        patientSign.image = patient.Signature1
        labelDate1.text = patient.dateToday
        date.text = patient.dateToday
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
