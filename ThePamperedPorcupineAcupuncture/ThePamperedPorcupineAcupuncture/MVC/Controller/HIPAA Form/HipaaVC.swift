//
//  ApicoectomyVC.swift
//  SmileDesignDentalSpa
//
//  Created by APPLE on 12/5/16.
//  Copyright © 2016 srs. All rights reserved.
//

import UIKit

class HippaVC: MCViewController {

    @IBOutlet weak var Sign1: SignatureView!
    @IBOutlet weak var date1: DateLabel!
    @IBOutlet weak var patientName: FormLabel!
    @IBOutlet weak var patientSignName: FormLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        date1.todayDate = patient.dateToday
        patientSignName.text = patient.fullName
        patientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues(){
        
        patient.Signature1 = Sign1.signatureImage()
    }
   
  

    
    @IBAction override func buttonBackAction() {
       saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func doneAction() {
     
       if !Sign1.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !date1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
       } else{
            saveValues()
        let nextVc = self.storyboard?.instantiateViewController(withIdentifier: "HipaaForm") as! HipaaForm
        nextVc.patient = self.patient
        self.navigationController?.pushViewController(nextVc, animated: true)
        }
    }


}

