//
//  PatientInfoViewController.swift
//  Always Great Smiles
//
//  Created by Berlin Raj on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: MCViewController {
   
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet weak var textFieldMiddleInitial: MCTextField!
    
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var labelDentist: UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    @IBOutlet weak var dropDownDentistName: BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldMiddleInitial.textFormat = .MiddleInitial
        textFieldDate.textFormat = .Date
        textFieldMonth.textFormat = .Month
        textFieldYear.textFormat = .Year

        if kDentistNames.count == 0 {
            dropDownDentistName.isHidden = true
            labelDentist.isHidden = true
        } else if kDentistNames.count > 1 {
            dropDownDentistName.items = kDentistNames
            dropDownDentistName.placeholder = isDentistNameNeeded ? "-- DENTIST NAME * --" : "-- DENTIST NAME --"
            dropDownDentistName.isHidden = false
            labelDentist.isHidden = true
        } else {
            labelDentist.text = kDentistNames[0].uppercased()
            dropDownDentistName.isHidden = true
            labelDentist.isHidden = false
        }
        
        labelPlace.text = kPlace
        labelDate.text = patient.dateToday
        
        textFieldFirstName.text = patient.visitorFirstName == nil ? "" : patient.visitorFirstName
        textFieldLastName.text = patient.visitorLastName == nil ? "" : patient.visitorLastName
        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        dropDownDentistName.selected = false
        self.view.endEditing(true)
        if kDentistNames.count > 1 && isDentistNameNeeded && dropDownDentistName.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT THE DENTIST NAME")
        } else if textFieldFirstName.isEmpty {
            self.showAlert("PLEASE ENTER PATIENT FIRST NAME")
        } else if textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER PATIENT LAST NAME")
        } else if invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else {
            
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.initial = textFieldMiddleInitial.text
            patient.dateOfBirth = getDateOfBirth()
            patient.dentistName = isDentistNameNeeded ? (kDentistNames.count > 1 ? dropDownDentistName.selectedOption! : (kDentistNames.count > 0 ? kDentistNames[0] : "")) : ""
            
            self.gotoNextForm()
        }
    }
    
    var isDentistNameNeeded: Bool {
        get {
            for (_, form) in patient.selectedForms.enumerated() {
                if kDentistNameNeededForms.contains(form.formTitle) {
                    return true
                }
            }
            return false
        }
    }
    
    func getDateOfBirth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let dob = dateFormatter.date(from: textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = kCommonDateFormat
        return dateFormatter1.string(from: dob).uppercased()
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}
