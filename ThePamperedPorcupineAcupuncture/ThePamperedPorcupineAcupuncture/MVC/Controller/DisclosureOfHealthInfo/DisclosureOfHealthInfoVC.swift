//
//  DisclosureOfHealthInfoVC.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/14/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class DisclosureOfHealthInfoVC: MCViewController {
    
    
    @IBOutlet weak var signatureWitness: SignatureView!
    @IBOutlet weak var signaturePatientOrRepresentative: SignatureView!
    //@IBOutlet weak var signatureOffice: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var textViewRestriction : MCTextView!
    @IBOutlet weak var textFieldSocialSecurityNo : MCTextField!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //textFieldSocialSecurityNo.textFormat = .SocialSecurity
        loadValue()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()
    {
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        
        if !patient.healthRestriction.isEmpty
        {
            textViewRestriction.text = patient.healthRestriction
            textViewRestriction.textColor = UIColor.black
        }
        
//        if !patient.socialSecurityNo.isEmpty
//        {
//            textFieldSocialSecurityNo.text = patient.socialSecurityNo
//        }
        labelPatientName.text = patient.fullName
        
        //Adding underline to the name of patient
        let stringAfterAddingName = NSMutableAttributedString(string: labelPatientName.text!)
        stringAfterAddingName.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange((labelPatientName.text!.indexDistance(of: patient.fullName.characters.first!))!, patient.fullName.characters.count))
        labelPatientName.attributedText = stringAfterAddingName
        
        labelDOB.text = patient.dateOfBirth
        
        //Adding underline to the date of birth
        let stringAfterAddingDOB = NSMutableAttributedString(string: labelDOB.text!)
        stringAfterAddingDOB.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange((labelDOB.text!.indexDistance(of: patient.dateOfBirth.characters.first!))!, patient.dateOfBirth.characters.count))
        labelDOB.attributedText = stringAfterAddingDOB
        
        
    }
    
    func saveValue()
    {
        
        if textViewRestriction.text != "PLEASE TYPE HERE"
        {
            patient.healthRestriction = (textViewRestriction
                .text?.isEmpty)! ? "" : textViewRestriction.text!
        }else{
            patient.healthRestriction = ""
        
        }
        
       // patient.socialSecurityNo = (textFieldSocialSecurityNo
           // .text?.isEmpty)! ? "" : textFieldSocialSecurityNo.text!

    
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        
        if !signaturePatientOrRepresentative.isSigned() || !signatureWitness.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }
//        else if !textFieldSocialSecurityNo.text!.isEmpty && !textFieldSocialSecurityNo.text!.isSocialSecurityNumber
//        {
//            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
//        }
        else
        {
            saveValue()
            let healthDisclosureForm = consentStoryBoard
                .instantiateViewController(withIdentifier: "kDisclosureOfHealthInfoForm") as! DisclosureOfHealthInfoForm
            healthDisclosureForm.patient = self.patient
            healthDisclosureForm.signPatient = signaturePatientOrRepresentative.signatureImage()
            healthDisclosureForm.signWitness = signatureWitness.signatureImage()
            self.navigationController?.pushViewController(healthDisclosureForm, animated: true)
        }
    }
    
    
    
}
