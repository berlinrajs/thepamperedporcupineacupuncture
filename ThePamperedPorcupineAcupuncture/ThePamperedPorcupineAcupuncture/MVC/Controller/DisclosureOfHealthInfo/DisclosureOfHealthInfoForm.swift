//
//  DisclosureOfHealthInfoForm.swift
//  ThePamperedPorcupineAcupuncture
//
//  Created by Manjusha Chembra on 2/14/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation

class DisclosureOfHealthInfoForm: MCViewController {
    
    
    var signPatient : UIImage!
    var signWitness : UIImage!
    
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var patientNameLabel
    : FormLabel!
    @IBOutlet weak var dateOfBirthLabel
    : FormLabel!
    @IBOutlet weak var socialSecurityLabel
    : FormLabel!
    @IBOutlet weak var signaturePatient: UIImageView!
    @IBOutlet weak var signatureWitness: UIImageView!
    
    @IBOutlet var labelRestriction: [UILabel]!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        patient.healthRestriction.setTextForArrayOfLabels(labelRestriction)
       // socialSecurityLabel.text = patient.socialSecurityNo.socialSecurityNumber
        dateOfBirthLabel.text = patient.dateOfBirth
        patientNameLabel.text = patient.fullName
        
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        
        signaturePatient.image = signPatient
        signatureWitness.image = signWitness
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
