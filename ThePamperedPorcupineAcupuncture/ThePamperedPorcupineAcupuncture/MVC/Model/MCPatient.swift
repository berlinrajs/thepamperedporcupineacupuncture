//
//  MCPatient.swift
//  MConsentForms
//
//  Created by Berlin Raj on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String!
    
    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial != nil && initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
    
    var visitorFirstName : String!
    var visitorLastName : String!
    
    var visitorFullName : String {
        return visitorFirstName + " " + visitorLastName
    }
    var visitingPurpose : String!
    
    var representativeRelationship : String = ""
    var healthRestriction : String = ""
    var socialSecurityNo : String = ""

    var Signature1: UIImage!
    
}

class Insurance: NSObject {
    var relation: Int!
    var companyName: String!
    var name: String!
    var employer: String!
    var group: String!
    var phoneNumber: String!
    var insuredID: String!
    var dateOfBirth: String!
    var otherRelation: String!
}


