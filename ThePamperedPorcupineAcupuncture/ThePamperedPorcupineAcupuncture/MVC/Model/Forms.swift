//
//  Forms.swift
//  MConsentForms
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var isToothNumberRequired : Bool!
    var toothNumbers : String!
    
    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (_ completion :(_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kInsuranceCard, kDrivingLicense, kConsentForms]
        
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnected ? false : true, formObj)
    }
    

    fileprivate class func getFormObjects (_ forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 5 : idx
            formObj.formTitle = form
             formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kWelcome,kPurposeOfTreatment,kArbitrationAgreement,kAcupunctureInformedConsent,kDisclosure,kHipaaForm], isSubForm:  true)
            }
            
            if formObj.formTitle == kFeedBack {
               formObj.index = forms.count + formList[consentIndex].subForms.count + 1
            }
            
            formList.append(formObj)
        }
        return formList
    }
    
}
