//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case Default = 0
    case SocialSecurity
    case ToothNumber
    case Phone
    case ExtensionCode
    case Zipcode
    case Number
    case Date
    case Month
    case Year
    case DateInCurrentYear
    case DateIn1980
    case Time
    case MiddleInitial
    case State
    case Amount
    case Email
    case SecureText
    case AlphaNumeric
    case Username 
}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kAppLoggedInKey = "kApploggedIn"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"

//VARIABLES
let kKeychainItemLoginName = "The Pampered Porcupine Acupuncture: Google Login"
let kKeychainItemName = "The Pampered Porcupine Acupuncture: Google Drive"
let kClientId = "447833572057-b5q46n2omit5j3j0ti896pbv0su7200o.apps.googleusercontent.com"
let kClientSecret = "moM0vo9bxcCWyEUWpUdMQLCO"
let kFolderName = "ThePamperedPorcupineAcupuncture"

let kDentistNames: [String] = ["NICOLE T. SMITH"]
let kDentistNameNeededForms = [kNewPatientSignInForm]
let kClinicName = "THE PAMPERED PORCUPINE ACUPUNCTURE"
let kAppName = "THE PAMPERED PORCUPINE ACUPUNCTURE"
let kPlace = "SCOTLAND, CT"
let kState = "CT"
let kAppKey = "mcPampered"
let kAppLoginAvailable: Bool = true
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"

//CONSENT FORMS
let kConsentForms = "CONSENT FORMS"
let kPurposeOfTreatment = "CONSENT FOR PURPOSES OF TREATMENT, PAYMENT AND HEALTH CARE OPERATION"
let kArbitrationAgreement = "ARBITRATION AGREEMENT"
let kAcupunctureInformedConsent = "ACUPUNCTURE INFORMED CONSENT TO TREAT"
let kWelcome = "WELCOME FORM"
let kDisclosure = "CONSENT TO THE USE AND DISCLOSURE OF HEALTH INFORMATION FOR TREATMENT, PAYMENT, OR HEALTHCARE OPERATIONS"
let kHipaaForm = "HIPAA INFORMATION AND CONSENT FORM"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kFeedBack = "CUSTOMER REVIEW FORM"
let kVisitorCheckForm = "VISITOR CHECK IN FORM"
// END OF FORMS

let toothNumberRequired: [String] = [""]
//Replace '""' with form names
let consentIndex: Int = 2


