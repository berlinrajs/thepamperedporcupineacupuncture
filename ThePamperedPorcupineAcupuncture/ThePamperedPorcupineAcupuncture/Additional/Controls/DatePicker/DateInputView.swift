//
//  DateInputView.swift
//  Secure Dental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateInputView: UIView {
    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!
    
    var arrayStates: [String]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), for: UIControlEvents.valueChanged)
        
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(datePicker)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .time ? "hh:mm a" : kCommonDateFormat
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .time ? "hh:mm a" : kCommonDateFormat
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
        textField.resignFirstResponder()
    }
    
    class func addDatePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        
        let dateString = "1 Jan \((Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date()))"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    class func addDatePickerForDateOfBirthTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        let dateString = "1 Jan 1980"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    
    class func addTimePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.time
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        dateListView.datePicker.setDate(dateFormatter.date(from: "12:00 am")!, animated: true)
    }
}
