//
//  PopupTextFieldDual.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PopupTextFieldDual: UIView {
    
    class func popUpView() -> PopupTextFieldDual {
        return Bundle.main.loadNibNamed("PopupTextFieldDual", owner: nil, options: nil)!.first as! PopupTextFieldDual
    }
    
    @IBOutlet weak var textField1: MCTextField!
    @IBOutlet weak var labelTitle1: UILabel!
    
    @IBOutlet weak var textField2: MCTextField!
    @IBOutlet weak var labelTitle2: UILabel!
    
    var completion:((PopupTextFieldDual, UITextField, UITextField)->Void)?
    var count : Int!
    
    func showInViewController(_ viewController: UIViewController?, WithTitle titles: (title1: String?, title2: String?), placeHolder : (placeholder1: String?, placeholder2: String?), textFormat: (format1: TextFormat, format2: TextFormat), completion : @escaping (_ popUpView: PopupTextFieldDual, _ textField1 : UITextField, _ textField2: UITextField) -> Void) {
        textField1.text = ""
        labelTitle1.text = titles.title1
        labelTitle2.text = titles.title2
        
        textField1.textFormat = textFormat.format1
        textField2.textFormat = textFormat.format2
        
        if let place = placeHolder.placeholder1 {
            textField1.placeholder = place
        }
        if let place = placeHolder.placeholder2 {
            textField2.placeholder = place
        }
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK() {
        completion?(self, self.textField1, self.textField2)
    }
    func close() {
        self.removeFromSuperview()
    }
}
